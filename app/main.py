from typing import Optional

from fastapi import FastAPI

from routers import read, link, tag, synonyme

app = FastAPI()

app.include_router(read.router)
app.include_router(link.router)
app.include_router(tag.router)
app.include_router(synonyme.router)

@app.get("/helloworld")
def read_root():
    return {"Hello": "World"}

@app.get("/version")
def get_version():
    f = open("../core/version.txt", "r")
    res = {}
    res["version"] = f.read().replace("\n", "")
    return res

import DB.manageDB as mdb

def numurl(url):
    res = ""
    for char in url:
        res += str(ord(char))
    return int(res)

if __name__ == "__main__":
    liste = mdb.allItem("lien")
    logfile = open("log/log.txt", "a")
    for link in liste:
        print(link["URL"])
        try:
            mdb.updateItem("lien", "URL", link["URL"], "id", hash(link["URL"]))
        except:
            print("FAIL")
            logfile.write(link["URL"])
    logfile.close()

from fastapi import APIRouter, Query
from typing import List, Optional
import DB.manageDB as mdb

router = APIRouter(
            prefix="/synonym",
            tags=["synonym", "read"]
            )


@router.get("/synList", tags=["search"])
async def syn_list():
    list = mdb.allSynonyme()
    res = {}
    res["nb_return"] = len(list)
    res["items"] = list
    return res


@router.get("/synByTag", tags=["search, tag"])
async def syn_by_tag(tag: str):
    ret = mdb.searchSynonymeByPrimKey(tag)
    res = {}
    clearRet = []
    res["nb_return"] = len(ret)
    for syn in ret:
        clearRet.append(syn["new"])
    res["items"] = clearRet
    return res

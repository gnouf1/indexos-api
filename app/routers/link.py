from fastapi import APIRouter, Query
from typing import List, Optional
import DB.manageDB as mdb

router = APIRouter(
            prefix="/link",
            tags=["link", "read"]
            )


@router.get("/searchLinkFromTags", tags=["search"])
async def search_Link_From_Tags(searchExp: str):
    dbRet = mdb.searchLinkFromTags(searchExp.split(" "), True)
    items = []
    for link in dbRet:
        dbRet_0 = mdb.searchLienByPrimKey(link["lien_url"])
        i = 0
        locDict = {}
        for item in dbRet_0[0]:
            locDict[dbRet_0[0].keys()[i]] = item
            i = i + 1
        items.append(locDict)

    res = {}
    res["nb_return"] = len(dbRet)
    res["items"] = items
    return res

from fastapi import APIRouter, Query
from typing import List, Optional
import DB.manageDB as mdb

router = APIRouter()


@router.get("/allItem", tags=["read", "all"])
async def all_items(tableName: str):
    dbRet = mdb.allItem(tableName)
    res = {}
    res["nb_return"] = len(dbRet)
    res["items"] = dbRet
    return res

@router.get("/simpleSearchItem", tags=["read", "search"])
async def simple_Search_Items(tableName: str, columnName: str, columnVal):
    dbRet = mdb.simpleItemSearch(tableName, columnName, columnVal)
    res = {}
    print(dbRet)
    res["nb_return"] = len(dbRet)
    res["items"] = dbRet
    return res

@router.get("/columnOccurence", tags=["read", "search"])
async def column_occurence(tableName: str, columnName: str):
    dbRet = mdb.colOccurence(columnName, tableName)
    res = {}
    res["nb_return"] = len(dbRet)
    res["items"] = dbRet
    return res

from fastapi import APIRouter, Query
from typing import List, Optional
import DB.manageDB as mdb

router = APIRouter(
            prefix="/tag",
            tags=["tag", "read"]
            )


@router.get("/toptag", tags=["sort"])
async def toptag(nb : Optional[int] = 10):
    dbRet = mdb.colOccurence("tag_value", "tagmap")

    if nb == -1 or len(dbRet) < 10:
        it = len(dbRet)

    dbRet.sort(key=lambda x: x[1], reverse=True)
    dbRet = dbRet[:nb]

    res = {}
    res["nb_return"] = len(dbRet)
    res["items"] = dbRet

    return res


@router.get("/relatedTo", tags=["search"])
async def related_to(tag: str):
    linkList = mdb.simpleItemSearch("tagmap", "tag_value", tag)
    rlTagList = []
    for link in linkList:
        for row in mdb.simpleItemSearch("tagmap", "lien_url", link["lien_url"]):
            if row["tag_value"] != tag and row["tag_value"] not in rlTagList:
                rlTagList.append(row["tag_value"])
    res = {}
    res["nb_return"] = len(rlTagList)
    res["items"] = rlTagList
    return res

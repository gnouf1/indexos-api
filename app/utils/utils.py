SQLKeywordsSet = {"ALTER", "SELECT", "WHERE", "DELETE", "DROP", "UPDATE"}

def avoidCharEscape(testStr):
    testStr = str(testStr)
    if testStr.isprintable():
        testStr = testStr.upper()
        for word in SQLKeywordsSet:
            if word in testStr:
                return False
        return True
    else:
        return False

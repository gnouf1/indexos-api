general:
	echo "Please precise wich mode you want"

preprod:
	$(shell uvicorn main:app --reload)

prod:
	$(shell cd app \n uvicorn main:app --host 0.0.0.0 --port 80)
